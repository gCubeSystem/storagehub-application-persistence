package org.gcube.storagehub;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.client.dsl.ListResolver;
import org.gcube.common.storagehub.client.dsl.ListResolverTyped;
import org.gcube.common.storagehub.client.dsl.OpenResolver;
import org.gcube.common.storagehub.model.Metadata;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.common.storagehub.model.service.Version;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StorageHubManagementTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(StorageHubManagementTest.class);
	
	@Test
	public void myTest() throws Exception {
		StorageHubManagement storageHubManagement = new StorageHubManagement();
		FolderContainer contextFolder = storageHubManagement.getContextFolder();
		logger.debug("Context Folder ID : {} - Name : {}", contextFolder.getId(), contextFolder.get().getName());
	}
	
	@Ignore
	@Test
	public void test() throws Exception {
		StorageHubManagement storageHubManagement = new StorageHubManagement();
		@SuppressWarnings("unused")
		OpenResolver openResolver = storageHubManagement.storageHubClient.open("");
		/*
		openResolver.asItem().delete();
		openResolver = storageHubManagement.storageHubClient.open("");
		openResolver.asItem().delete();
		openResolver = storageHubManagement.storageHubClient.open("");
		openResolver.asItem().delete();
		openResolver = storageHubManagement.storageHubClient.open("");
		openResolver.asItem().delete();
		*/
	}
	
	@Ignore
	@Test
	public void testPublicLink() throws Exception {
		@SuppressWarnings("unused")
		String urlString = "https://data.dev.d4science.org/shub/E_WE9XM3Vad1orRGNhZVJJY0NybkFQR1BzRzZKUndPeVdLYTk4b1Y2MmVhVE9oZFYvM2hBb1JKRkluT1JQR1RZdQ==";
	}
	
	@Ignore
	@Test
	public void listFolders() throws Exception {
		String c = "/gcube";
//		c = "/pred4s/preprod/GRSF_Pre";
//		c = "/d4science.research-infrastructures.eu/FARM/GRSF_Pre";
//		c = "/d4science.research-infrastructures.eu/FARM/GRSF_Admin";
//		c = "/d4science.research-infrastructures.eu/FARM/GRSF";
		List<String> contexts = new ArrayList<>();
		contexts.add(c);
		
		for(String context : contexts) {
			ContextTest.setContextByName(context);
			
			StorageHubManagement storageHubManagement = new StorageHubManagement();
			
//			FolderContainer root = storageHubManagement.getWorkspaceRoot();
//			storageHubManagement.tree(root);
			
			FolderContainer contextFolder = storageHubManagement.getContextFolder();
//			storageHubManagement.tree(contextFolder);
			
			ListResolverTyped listResolverTyped = contextFolder.list();
			List<ItemContainer<? extends Item>> containers = listResolverTyped.includeHidden().getContainers();
			for(ItemContainer<? extends Item> itemContainer : containers) {
				Item item = itemContainer.get();
				String name = item.getName();
				ContainerType containerType = itemContainer.getType();
				if(containerType==ContainerType.FOLDER) {
					if(item.isHidden()) {
						storageHubManagement.logItem(itemContainer);
					}
					if(name.compareTo(".catalogue")==0 || name.compareTo("service-account-gcat")==0 || name.compareTo("service-account-grsf-publisher")==0) {
						logger.info("Catalogue folder found");
						storageHubManagement.logItem(itemContainer);
						// storageHubManagement.tree((FolderContainer) itemContainer);
						if(name.compareTo(".catalogue")==0) {
							// storageHubManagement.tree((FolderContainer) itemContainer);
							// itemContainer.forceDelete();
						}
						if(name.compareTo("service-account-grsf-publisher")==0) {
							// storageHubManagement.tree((FolderContainer) itemContainer);
							// itemContainer.forceDelete();
						}
						if(name.compareTo("service-account-gcat")==0) {
							// storageHubManagement.tree((FolderContainer) itemContainer);
							// itemContainer.forceDelete();
						}
					}
				}
			}
		}
		
	}
	
	@Ignore
	@Test
	public void emptyTrash() throws Exception {
		String c = GCUBE;
//		c = ContextTest.ROOT_PRE;
//		c = ContextTest.ROOT_PROD;
		ContextTest.setContextByName(c);
		StorageHubManagement storageHubManagement = new StorageHubManagement();
		storageHubManagement.getStorageHubClient().emptyTrash();
	}
	
	@Ignore
	@Test
	public void getFileInfo() throws Exception {
		StorageHubManagement storageHubManagement = new StorageHubManagement();
		String id = "";
		OpenResolver openResolver = storageHubManagement.storageHubClient.open(id);
		FileContainer fileContainer = (FileContainer) openResolver.asFile();
		storageHubManagement.logItem(fileContainer);
		
		/*
		ListResolver listResolver = fileContainer.getAnchestors();
		List<ItemContainer<? extends Item>> itemContainers = listResolver.getContainers();
		for(ItemContainer<? extends Item> itemContainer : itemContainers) {
			logger.debug("{}", itemContainer.get().getName());
		}
		*/
		
		Metadata metadata = fileContainer.get().getMetadata();
		Map<String,Object> map = metadata.getMap();
		logger.debug("{}", map);
		
		List<Version> versions = fileContainer.getVersions();
		for(Version version : versions){
			logger.debug("Version {} {}", version.getId(), version.getName());
		}
	}

	@Ignore
	@Test
	public void getFileInfoViaDirectoryListing() throws Exception {
		StorageHubManagement storageHubManagement = new StorageHubManagement();
		String id = "";
		OpenResolver openResolver = storageHubManagement.storageHubClient.open(id);
		FolderContainer folderContainer = (FolderContainer) openResolver.asFolder();
		String filename = "";
		ListResolver listResolver = folderContainer.findByName(filename);
		List<ItemContainer<? extends Item>> itemContainers = listResolver.withMetadata().getContainers();
		for(ItemContainer<? extends Item> itemContainer : itemContainers) {
		
			FileContainer fileContainer = (FileContainer) itemContainer;
			storageHubManagement.logItem(fileContainer);
			
			/*
			ListResolver listResolver = fileContainer.getAnchestors();
			List<ItemContainer<? extends Item>> itemContainers = listResolver.getContainers();
			for(ItemContainer<? extends Item> itemContainer : itemContainers) {
				logger.debug("{}", itemContainer.get().getName());
			}
			*/
			
			Metadata metadata = fileContainer.get().getMetadata();
			Map<String,Object> map = metadata.getMap();
			logger.debug("{}", map);
			
			List<Version> versions = fileContainer.getVersions();
			for(Version version : versions){
				logger.debug("Version {} {}", version.getId(), version.getName());
			}
		}
		
	}
	
}
