package org.gcube.storagehub;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.storagehub.model.Metadata;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class MetadataMatcher {
	
	public static final String GENERATING_METADATA_NAME = "Generating Metadata Name";
	public static final String GENERATING_METADATA_VERSION = "Generating Metadata Version";
	
	protected final String metadataName;
	protected final String metadataVersion;
	protected final String id;
	
	protected MetadataMatcher(String metadataName, String metadataVersion, String id) {
		this.metadataName = metadataName;
		this.metadataVersion = metadataVersion;
		this.id = id;
	}
	
	public abstract boolean check(Metadata metadata);
	
	protected Map<String,Object> getBaseMetadataMap() {
		Map<String,Object> map = new HashMap<>();
		map.put(MetadataMatcher.GENERATING_METADATA_NAME, metadataName);
		map.put(MetadataMatcher.GENERATING_METADATA_VERSION, metadataVersion);
		return map;
	}
	
	protected abstract Map<String,Object> getSpecificMetadataMap();
	
	public Metadata getMetadata() {
		Map<String, Object> map = getBaseMetadataMap();
		map.putAll(getSpecificMetadataMap());
		Metadata metadata = new Metadata(map);
		return metadata;
	}
	
}
