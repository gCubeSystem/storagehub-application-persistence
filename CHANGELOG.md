This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for StorageHub Application Persistence

## [v3.5.0]

- Enhanced gcube-bom version to 3.4.1 [#27999]
- Updated maven-parent version to 1.2.0 


## [v3.4.0]

- The persistence of file already stored in the workspace can be ensured by using the copy provided by storage-hub #27736
- The removal of a file persited is resolved using the public link #27952

## [v3.3.0]

- Added GENERATING_APPLICATION_NAME and GENERATING_APPLICATION_METADATA_VERSION to be used in Metadata


## [v3.2.0]

- Added dependency to be able to compile with JDK 11


## [v3.1.0]

- Enhanced range of storagehub-client-library to 2.0.0,3.0.0-SNAPSHOT [#22777]


## [v3.0.0]

- Removed code which is now part of authorization-utils [#22472]


## [v2.0.0] [r5.4.0] - 2021-08-05

- Removed home library [#21435]  
- Switched HTTP requests gxHTTP 2.0.0 [#19283]
- Switched gcube-bom to 2.0.0 [#19283]
- Migrating to the IAM [#22472]


## [v1.3.1] [r4.20.0] - 2020-02-14

- Added method to retrieve a persisted file


## [v1.3.0] [r4.18.0] - 2019-12-20

- Changed the pom and distro files to comply with new release procedure
- Reintroduced Home Library to cover cases not managed by storage-hub

## [v1.2.0] [r4.15.0] - 2019-11-06

- Removed Home Library usage [#17229]


## [v1.1.0] [r4.14.0] - 2019-05-27

- Catching new exceptions raised from Storage Hub clients


## [v1.0.0] [r4.13.1] - 2019-02-26

- First Release

